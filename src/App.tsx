/* eslint-disable consistent-return */
import {
  Stack,
} from '@mantine/core';
import { useEffect, useState } from 'react';
import Papa from 'papaparse';
import LineChart from './components/linechart';
import { PieChart } from './components/piechart';

function App() {
  const [data, setData] = useState([]);
  const [isloading, setLoading] = useState(false);

  const games = new Set();
  const dates = new Set();
  const countries = new Set();

  const changeHandler = (event: any) => {
    Papa.parse(event.target.files[0], {
      header: true,
      skipEmptyLines: true,
      complete(results: any) {
        setData(results.data);
        setLoading(true);
      },
    });
  };

  useEffect(() => {
    if (!data) {
      return null;
    }

    data.map((item) => {
      games.add(item.App);
      dates.add(item.Date);
      countries.add(item.Country);
    });
    console.log(Array.from(games).map((item) => console.log(item)));
    console.log(Array.from(dates));
    console.log(Array.from(countries));
  }, [isloading]);
  console.log(data);
  return (
    <Stack align="center" w="100vw">

      <input
        placeholder="Upload CSV"
        type="file"
        name="file"
        accept=".csv"
        onChange={changeHandler}
      />
      {isloading ? (
        <>
          <LineChart />
          <PieChart />
        </>
      ) : <p>...load file first</p>}
    </Stack>
  );
}

export default App;
