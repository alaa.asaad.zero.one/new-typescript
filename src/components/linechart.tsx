import React from 'react';
import { Line } from 'react-chartjs-2';
import {
     Chart as ChartJS,
     CategoryScale,
     LinearScale,
     PointElement,
     LineElement,
     Title,
     Tooltip,
     Legend,
} from 'chart.js';

ChartJS.register(
     CategoryScale,
     LinearScale,
     PointElement,
     LineElement,
     Title,
     Tooltip,
     Legend,
);
const options = {
     responsive: true,
     plugins: {
          legend: {
               position: 'top' as const,
          },
          title: {
               display: true,
               text: 'Chart.js Line Chart',
          },
     },
};

function LineChart() {
     const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
     const data = {
          labels,
          datasets: [
               {
                    label: 'Dataset 1',
                    data: labels.map(() => Math.random()),
                    borderColor: 'rgb(255, 99, 132)',
                    backgroundColor: 'rgba(255, 99, 132, 0.5)',
               },
               {
                    label: 'Dataset 2',
                    data: labels.map(() => Math.random()),
                    borderColor: 'rgb(53, 162, 235)',
                    backgroundColor: 'rgba(53, 162, 235, 0.5)',
               },
          ],
     };
     return (
          <div style={{ width: '60vw', height: '60vh' }}>
               <Line options={options} data={data} />
          </div>
     );
}

export default LineChart;
